#include "stm32f4xx_hal.h"
#include <errno.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/unistd.h>

#define PRINTF_UART huart2

extern UART_HandleTypeDef PRINTF_UART;

size_t __malloc_margin = 256;
char *__brkval;

static void print_char(char chr) {
	HAL_UART_Transmit(&PRINTF_UART, (uint8_t*)&chr, 1, 1000);
}

void *_sbrk_r(struct _reent *r, ptrdiff_t incr) {
	extern char end;
	
	if(__brkval == 0)
		__brkval = &end;
		
	if(__brkval + incr > (char*)__get_MSP() - __malloc_margin) {
		r->_errno = ENOMEM;
		return(void*)-1;
	}
	void *ret = __brkval;
	__brkval +=incr;
	
	return ret;
		
}

static char wait_for_char(void) {
	uint8_t rxed;
	HAL_StatusTypeDef res;
	
	do {
		res = HAL_UART_Receive(&PRINTF_UART, &rxed,1, HAL_MAX_DELAY);
	} while (res != HAL_OK);
	return rxed;
	
}
ssize_t _read_r(struct _reent *r, int fd, const void *ptr, size_t len) {
	//int cntr = len;
	char *pTemp = (char*)ptr;
	for(int i=0; i<len; i++) {
		*pTemp = wait_for_char();
		pTemp++;
	}
	return len;
}

ssize_t _write_r(struct _reent *r, int fd, const void *ptr, size_t len) {
	int cntr = len;
	char *pTemp = (char*)ptr;
	while(cntr--)
		print_char(*pTemp++);
	return len;
}